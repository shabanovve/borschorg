package ru.dz.borschorg.db.dao;

import ru.dz.borschorg.db.entity.Quotation;

import java.util.List;

/**
 * Created by Vladimir Shabanov on 06/01/16.
 */
public interface QuotationDAO {

    List<Quotation> getAllQuotaion();

    Long add(Quotation quotation);

    Long getCommonQuantity();

    List<Quotation> getConstrainQuantity(int pageNumber, int itemsPerPage);
}
