package ru.dz.borschorg.db.dao.impl;

import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.dz.borschorg.db.dao.QuotationDAO;
import ru.dz.borschorg.db.entity.Quotation;
import ru.dz.borschorg.db.util.Statistics;

import java.util.List;

/**
 * Created by Vladimir Shabanov on 06/01/16.
 */
@Transactional
@Repository("quotationDAOImpl")
public class QuotationDAOImpl extends BaseDAO implements QuotationDAO{

    @Autowired
    private Statistics statistics;

    @Override
    public List<Quotation> getAllQuotaion() {
        return getSession().createCriteria(Quotation.class).list();
    }

    @Override
    public Long add(Quotation quotation) {
        getSession().persist(quotation);
        return quotation.getId();
    }

    @Override
    public Long getCommonQuantity() {
        Long result = (Long) getSession()
                .createCriteria(Quotation.class)
                .setProjection(Projections.rowCount())
                .uniqueResult();
        return result;
    }

    @Override
    @Cacheable(value="quotationCache", key="#pageNumber")
    public List<Quotation> getConstrainQuantity(int pageNumber, int itemsPerPage) {
        statistics.incDBRequest();
        List<Quotation> result = getSession()
                .createCriteria(Quotation.class)
                .setFirstResult(pageNumber * itemsPerPage)
                .setMaxResults(itemsPerPage)
                .list();
        return result;
    }
}
