package ru.dz.borschorg.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Vladimir Shabanov on 06/01/16.
 */
@Entity
public class Quotation implements Serializable{
    @Id
    @GeneratedValue
    private Long id;
    @Column(length = 10485760)
    private String text;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
