package ru.dz.borschorg.db.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Vladimir Shabanov on 10/01/16.
 */
@Slf4j
@Component
public class Statistics {

    private static AtomicLong daoRequestCount = new AtomicLong();
    private static AtomicLong dbRequestCount = new AtomicLong();
    private static Long oldDaoCount;
    private static Long oldDBCount;

    public static Long getDaoRequestCount() {
        return daoRequestCount.get();
    }

    public static Long getDbRequestCount() {
        return dbRequestCount.get();
    }

    public void incDaoRequest() {
        daoRequestCount.incrementAndGet();
    }

    public void incDBRequest() {
        dbRequestCount.incrementAndGet();
    }

    @Scheduled(fixedDelay = 5000)
    public void showStaticrics() {
        boolean somethingChanged = !getDaoRequestCount().equals(oldDaoCount) || !getDbRequestCount().equals(oldDBCount);
        if (somethingChanged) {
            log.warn(
                    "\ndaoRequestCount=" + getDaoRequestCount() +
                            "\ndbRequestCount=" + getDbRequestCount()
            );
            oldDBCount = getDbRequestCount();
            oldDaoCount = getDaoRequestCount();
        }
    }
}
