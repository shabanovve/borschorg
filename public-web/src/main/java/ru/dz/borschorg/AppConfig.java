package ru.dz.borschorg;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


/**
 * Created by Vladimir Shabanov on 06/01/16.
 */
@Configuration
@ComponentScan("ru.dz.borschorg")
public class AppConfig {

}
