package ru.dz.borschorg.bean;

/**
 * Created by Vladimir Shabanov on 09/01/16.
 */
public class Page {
    private int number;
    private String href;
    private boolean isCurrent;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean getIsCurrent() {
        return isCurrent;
    }

    public void setCurrent(boolean current) {
        isCurrent = current;
    }
}
