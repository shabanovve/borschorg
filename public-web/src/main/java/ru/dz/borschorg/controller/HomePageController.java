package ru.dz.borschorg.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.dz.borschorg.bean.Page;
import ru.dz.borschorg.db.entity.Quotation;
import ru.dz.borschorg.pagination.Paginator;
import ru.dz.borschorg.service.QuotationService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Vladimir Shabanov on 06/01/16.
 */
@Controller
@RequestMapping(value = HomePageController.ROOT)
public class HomePageController {
    public static final String ROOT = "/";
    public static final int ITEMS_PER_PAGE = 50;

    @Autowired
    private QuotationService quotationService;

    @RequestMapping
    public String showHomePage(
            Model model,
            HttpServletRequest request
    ) {
        renderHomePage(model, 1, request.getRequestURL().toString());
        return "homePage";
    }


    @RequestMapping(params = {"page"})
    public String showHomePage(
            Model model,
            @RequestParam(value = "page", defaultValue = "1") int pageNumber,
            HttpServletRequest request
    ) {
        renderHomePage(model, pageNumber, request.getRequestURL().toString());
        return "homePage";
    }

    private void renderHomePage(Model model, int pageNumber, String url) {
        List<Quotation> quotations = quotationService.getConstrainQuantity(pageNumber,ITEMS_PER_PAGE);
        model.addAttribute("msg", "Borschorg is started");
        model.addAttribute("quotations", quotations);
        model.addAttribute("pages", definePages(pageNumber, url));
    }

    private List<Page> definePages(int pageNumber, String url) {
        Long longValue = quotationService.getCommonQuantity() / ITEMS_PER_PAGE;
        int maxPage = longValue.intValue();
        return Paginator.preparePages(pageNumber, maxPage, url);
    }

}
