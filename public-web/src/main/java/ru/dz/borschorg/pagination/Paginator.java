package ru.dz.borschorg.pagination;

import ru.dz.borschorg.bean.Page;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vladimir Shabanov on 09/01/16.
 */
public class Paginator {
    public static final int RANGE_ITEMS_COUNT = 5;


    public static List<Page> preparePages(int pageNumber, int maxPage, String url){
        boolean leftCase = pageNumber <= RANGE_ITEMS_COUNT + 1;//текущая страница ближе к предыдущем страницам, крайний левый случай
        boolean rightCase = maxPage - pageNumber < RANGE_ITEMS_COUNT;//текущая страница ближе к следущий страницам, крайний правый случай
        boolean commonCase = !leftCase && !rightCase;//текущая страница находится точно между предыдущими и последущими страницами

        List<Page> result = new ArrayList<Page>();

        if (leftCase) {
            for (int i = 1; i < pageNumber; i++) {
                result.add(createPage(url, i));
            }
            result.add(createPage(url, pageNumber, true));
            int different = RANGE_ITEMS_COUNT - pageNumber + 1;
            for (int i = pageNumber + 1; i <= pageNumber + RANGE_ITEMS_COUNT + different; i++) {
                result.add(createPage(url, i));
            }
        }

        if (commonCase) {
            for (int i = pageNumber - RANGE_ITEMS_COUNT; i < pageNumber; i++) {
                result.add(createPage(url, i));
            }
            result.add(createPage(url, pageNumber, true));
            for (int i = pageNumber + 1; i <= pageNumber + RANGE_ITEMS_COUNT; i++) {
                result.add(createPage(url, i));
            }
        }

        if (rightCase) {
            for (int i = maxPage - 2 * RANGE_ITEMS_COUNT; i < pageNumber; i++) {
                result.add(createPage(url, i));
            }
            result.add(createPage(url, pageNumber, true));
            for (int i = pageNumber + 1; i <= maxPage; i++) {
                result.add(createPage(url, i));
            }
        }
        return result;
    }

    private static Page createPage(String url, int i) {
        return createPage(url, i, false);
    }

    private static Page createPage(String url, int i, boolean isCurrent) {
        Page page = new Page();
        page.setNumber(i);
        page.setHref(url + "?page=" + i);
        page.setCurrent(isCurrent);
        return page;
    }


}
