package ru.dz.borschorg.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import ru.dz.borschorg.db.dao.QuotationDAO;
import ru.dz.borschorg.db.entity.Quotation;
import ru.dz.borschorg.db.util.Statistics;

import java.util.List;

/**
 * Created by Vladimir Shabanov on 06/01/16.
 */
@Service
public class QuotationService {

    @Autowired
    @Qualifier("quotationDAOImpl")
    private QuotationDAO quotationDAO;

    @Autowired
    private Statistics statistics;


    public List<Quotation> getAllQuotqtions(){
        return quotationDAO.getAllQuotaion();
    }

    public Long getCommonQuantity() {
        return quotationDAO.getCommonQuantity();
    }

    public List<Quotation> getConstrainQuantity(int pageNumber, int itemsPerPage) {
        statistics.incDaoRequest();
        return quotationDAO.getConstrainQuantity(pageNumber,itemsPerPage);
    }
}
