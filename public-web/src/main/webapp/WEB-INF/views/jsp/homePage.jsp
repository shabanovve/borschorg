<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ru">
<head>
<title>Borschorg</title>
</head>

<c:out value="${msg}"/>

<jsp:include page="pagination.jsp"/>

<c:if test="${not empty quotations}">
    <ul>
        <c:forEach var="quotation" items="${quotations}">
            <li>${quotation.text}</li>
        </c:forEach>
    </ul>

</c:if>

</body>
</html>