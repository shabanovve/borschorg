<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<span>
        <c:forEach var="page" items="${pages}">
            <c:choose>
                <c:when test="${page.isCurrent}">
                    <span class="page current">${page.number}</span>
                </c:when>
                <c:otherwise>
                    <a class="page" href="${page.href}">${page.number}</a>
                </c:otherwise>
            </c:choose>
        </c:forEach>
</span>