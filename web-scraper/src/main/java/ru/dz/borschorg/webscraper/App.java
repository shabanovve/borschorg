package ru.dz.borschorg.webscraper;

/**
 * Created by Vladimir Shabanov on 07/01/16.
 */
public class App {

    public static void main(String[] args){
        new Scraper().start();
    }
}
