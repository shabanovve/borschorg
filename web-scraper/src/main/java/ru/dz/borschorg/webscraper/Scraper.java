package ru.dz.borschorg.webscraper;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.dz.borschorg.db.dao.QuotationDAO;
import ru.dz.borschorg.db.entity.Quotation;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Vladimir Shabanov on 07/01/16.
 */
@Slf4j
public class Scraper {
    private static final int MAX_PAGE = 300;
    private static final String START_URL = "http://bash.im/";
    private WebDriver driver = new HtmlUnitDriver();
    private QuotationDAO quotationDAO;

    public void start() {
        log.info("Scraper is started");
        init();

        if (quotationDAO != null) {
            String url = START_URL;
            for (int pageCount = 1; pageCount <= MAX_PAGE; pageCount++) {
                scrapOnePage(url);
                url = defineNextPageUrl();
                pause(10000);
            }
        } else {
            log.error("QuotationDAO is not defined");
        }

    }

    private void pause(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String defineNextPageUrl() {
        String url = "";
        List<WebElement> elements = driver.findElements(By.cssSelector(".pager form *"));
        WebElement element = null;
        for (Iterator<WebElement> iterator = elements.iterator(); iterator.hasNext(); ) {
            element = iterator.next();
            if (element.toString().contains("<input") && iterator.hasNext()) {
                element = iterator.next();
                url = element.getAttribute("href");
                break;
            }
        }
        return url;
    }

    private void scrapOnePage(String url) {
        driver.get(url);
        List<WebElement> elements = driver.findElements(By.className("text"));
        saveAll(quotationDAO, elements);
    }

    private void init() {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring.xml");
        quotationDAO = (QuotationDAO) applicationContext.getBean("quotationDAOImpl");
    }

    private void saveAll(QuotationDAO quotationDAO, List<WebElement> elements) {
        for (WebElement element : elements) {
            String text = element.getText().toString();
            log.info(text);
            Quotation quotation = new Quotation();
            quotation.setText(text);
            quotationDAO.add(quotation);
        }
    }

}
