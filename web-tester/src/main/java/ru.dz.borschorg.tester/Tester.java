package ru.dz.borschorg.tester;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Vladimir Shabanov on 09/01/16.
 */
@Slf4j
public class Tester {

    private static final int THREAD_MAX = 200;
    private static final AtomicLong requestCount = new AtomicLong();
    private static AtomicInteger workerCount = new AtomicInteger();

    public void start() {
        final CountDownLatch startCountDownLatch = new CountDownLatch(THREAD_MAX);
        for (int i = 1; i <= THREAD_MAX; i++) {
            new Thread(new Worker(startCountDownLatch, requestCount, workerCount)).start();
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                await();

                long start = System.currentTimeMillis();
                long milliseconds = 0L, end = 0L, rps = 0L;

                while (workerCount.get() > 0) {
                    pause();
                    end = System.currentTimeMillis();
                    milliseconds = end - start;
                    rps = requestCount.get() / (milliseconds / 1000);
                    log.info(rps + " request per second");
                }

                rps = requestCount.get() / (milliseconds / 1000);
                log.info("\nSummary:" +
                        "\n requestCount=" + requestCount.get() +
                        "\n milliseconds=" + milliseconds +
                        "\n request per second=" + rps
                );
            }
            private void await() {
                try {
                    startCountDownLatch.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            private void pause() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }
}
