package ru.dz.borschorg.tester;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Vladimir Shabanov on 09/01/16.
 */
@Slf4j
public class Worker implements Runnable {
    private static final String START_URL = "http://localhost:8080/";
    private WebDriver driver = new HtmlUnitDriver();
    private CountDownLatch startCountDownLatch;
    private AtomicLong requestCount;
    private AtomicInteger workerCount;

    public Worker(CountDownLatch startCountDownLatch, final AtomicLong requestCount, final AtomicInteger workerCount) {
        this.startCountDownLatch = startCountDownLatch;
        this.requestCount = requestCount;
        this.workerCount = workerCount;
    }

    @Override
    public void run() {
        String url = START_URL;
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        workerCount.incrementAndGet();
        startCountDownLatch.countDown();
        await();

        do {
            driver.get(url);
            url = defineNextPageUrl();
            requestCount.incrementAndGet();
        } while (!url.isEmpty());

        workerCount.decrementAndGet();

    }

    private void await() {
        try {
            startCountDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String defineNextPageUrl() {
        String url = "";
        List<WebElement> elements = driver.findElements(By.className("page"));
        WebElement element = null;
        for (Iterator<WebElement> iterator = elements.iterator(); iterator.hasNext(); ) {
            element = iterator.next();
            if (element.toString().contains("current") && iterator.hasNext()) {
                element = iterator.next();
                url = element.getAttribute("href");
                break;
            }
        }
        return url;
    }
}
